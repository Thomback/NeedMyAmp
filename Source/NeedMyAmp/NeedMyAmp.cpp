// Copyright Epic Games, Inc. All Rights Reserved.

#include "NeedMyAmp.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, NeedMyAmp, "NeedMyAmp" );
